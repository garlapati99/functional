package finalsection;

import java.util.function.Consumer;
import java.util.function.Function;

public class main {

    public static void main(String[] args) {
        hello("John", null, value -> {
            System.out.println("no lastname provided for "+value);
        });
    }

    static void hello(String firstname, String lastname, Consumer<String> callback){
        System.out.println(firstname);
        if (lastname != null) {
            System.out.println(lastname);
        } else {
            callback.accept(firstname);
        }
    }

//    function hello(firstName, lastName, callback){
//        console.log(firstname);
//        if(lastname){
//            console.log(lastname)
//        } else {
//            callback();
//        }
//    }
}
