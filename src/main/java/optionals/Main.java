package optionals;

import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Supplier;

public class Main {

    public static void main(String[] args) {
//        Consumer<String> stringConsumer = email -> System.out.println("sending email to  " + email);
//        Optional.ofNullable("Hello")
//                .ifPresent(stringConsumer);

        Optional.ofNullable(null)
                .ifPresentOrElse(email -> System.out.println("Sending email to  "+ email)
                , () -> System.out.println("Cannot send email"));
    }
}
