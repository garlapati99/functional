package combinationpattern;

import java.time.LocalDate;

import static combinationpattern.CustomerRegistrationValidator.*;


public class Main {


    public static void main(String[] args) {

        Customer customer = new Customer(
                "Alice",
                "alice@gmail.com",
                "+6598511007",
                LocalDate.of(2000, 01, 01)
        );



        //System.out.println(new CustomerValidatorService().isValid(customer));

        //if valid we can store cusotmer in db

        //using combinator pattern

        ValidationResult result = isEmailValid()
                .and(isPhoneNumberValid())
                .and(isAnAdult())
                .apply(customer);

        System.out.println(result);

        if(result!=ValidationResult.SUCCESS){
            throw new IllegalStateException(result.name());
        }


    }
}
