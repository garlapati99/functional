package combinationpattern;

import java.time.LocalDate;
import java.time.Period;

public class CustomerValidatorService {

    private boolean isemailValid(String email){
        return email.contains("@");
    }

    private boolean isphoneNumberValid(String phoneNumber){
        return phoneNumber.startsWith("+65");
    }

    private boolean isAdult(LocalDate dob) {
        return Period.between(dob, LocalDate.now()).getYears() > 16;
    }

    public boolean isValid(Customer customer){
        return isemailValid(customer.getEmail())&&
                isphoneNumberValid(customer.getPhoneNumber())&&
                isAdult(customer.getDob());

    }

 }
