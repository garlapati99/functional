package functioninterface;

import java.util.function.BiConsumer;
import java.util.function.Consumer;

public class _Consumer {
    public static void main(String[] args) {
        Customer cust = new Customer("maria", "98511007");
        greetCustomer(cust);
        greetCustomerConsumer.accept(cust);
        System.out.println("Bi Consumer");
        greetCustomerConsumerV2.accept(cust,false);
    }



    static void greetCustomer(Customer customer){
        System.out.println("Hello " + customer.customerName + " thanks for registering "+ customer.customerPhoneNumber);
    }

    static Consumer<Customer> greetCustomerConsumer = customer ->
            System.out.println("Hello " + customer.customerName + " thanks for registering "+ customer.customerPhoneNumber);

    static BiConsumer<Customer, Boolean> greetCustomerConsumerV2 = (customer, showPhonenumber) ->
            System.out.println("Hello " + customer.customerName +
                    " thanks for registering "
                    + (showPhonenumber ? customer.customerPhoneNumber : "*********"));

    static class Customer{
        private final String customerName;
        private final String customerPhoneNumber;

        Customer(String customerName, String customerPhoneNumber) {
            this.customerName = customerName;
            this.customerPhoneNumber = customerPhoneNumber;
        }
    }
}
