package functioninterface;

import java.util.function.BiFunction;
import java.util.function.Function;

public class _Function {

    public static void main(String[] args) {
        int increment = increment(1);
        System.out.println(increment);

        System.out.println("Functional Interface");

        Integer increment2 = incrementByOneFunction.apply(10);
        System.out.println(increment2);
        System.out.println("Multiple Functions");
        Integer increment3 = incrementByOneFunction.andThen(multipleBy10Function).apply(2);
        System.out.println(increment3);

        System.out.println("normal function");
        int increment6 = incrementByOneAndMultiply(3, 10);
        System.out.println(increment6);


        System.out.println("Bifunction");

        int increment5 = incrementByOneAndMultiply(3, 10);
        System.out.println(increment5);

    }

    static int increment(int number){
        return number+1;
    }

    static Function<Integer, Integer> incrementByOneFunction = number -> number + 1;
    static Function<Integer, Integer> multipleBy10Function = number -> number * 10;

    //Bifunction

    static int incrementByOneAndMultiply(int number, int numbertomultiply){
        return (number+1)*numbertomultiply;
    }

    BiFunction<Integer, Integer, Integer> incrementbyoneandmultiplybifunction =
            (numbertoincrementbyone, numbertomultiplyby) -> (numbertoincrementbyone +1)*numbertomultiplyby;

}
