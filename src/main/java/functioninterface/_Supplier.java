package functioninterface;

import java.util.function.Supplier;

public class _Supplier {
    public static void main(String[] args) {
        System.out.println(getDBConnectionUrl());
        System.out.println("Supplier");
        System.out.println(getDBConnectionUrlSupplier.get());
    }

    static String getDBConnectionUrl (){
        return "jdbc://localhost5432/users";
    }

    //supplier
    static Supplier<String> getDBConnectionUrlSupplier = () -> "jdbc://localhost5432/users";
}
