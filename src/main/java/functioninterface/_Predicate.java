package functioninterface;

import java.util.function.BiPredicate;
import java.util.function.Predicate;

public class _Predicate {

    public static void main(String[] args) {

        System.out.println("98511007");
        System.out.println("78944033");

        boolean phoneNumberValidate = isPhoneNumberValidate("98511007");
        System.out.println(phoneNumberValidate);
        System.out.println(isPhoneNumberValidate("78944033"));

        System.out.println("Predicate");

        System.out.println(isPhoneNumberValidatePredicate.test("98511007"));
        System.out.println(isPhoneNumberValidatePredicate.test("78944033"));
        System.out.println(isPhoneNumberValidatePredicate.test("7894403"));

        System.out.println("Two predicate");
        System.out.println(isPhoneNumberValidatePredicate.and(containsNumber3).test("98511008"));

    }

    //normal function
    static boolean isPhoneNumberValidate(String phonenumber){
        return phonenumber.startsWith("98") && phonenumber.length() == 8;
    }

    //java 11 functions

    static Predicate<String> isPhoneNumberValidatePredicate = phonenumber ->
            phonenumber.startsWith("98") && phonenumber.length() == 8;

    static Predicate<String> containsNumber3 = phonenumber ->
            phonenumber.contains("7");

    //Bi Predicate
}
